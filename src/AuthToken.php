<?php

namespace harpya\api_auth;

/**
 * Trait AuthToken
 * @package harpya\api_auth
 */
trait AuthToken
{
    /**
     * @return string
     */
    public function createNewToken()
    {
        $newValue = (rand(100, 500) * intval($this->getPrime()));
        $s = $this->getSalt() . ":$newValue";
        $hash = $this->getHash($s);
        $s .= ":$hash";

        return base64_encode($s);
    }

    /**
     * @param $data
     * @return string
     */
    protected function getHash($data)
    {
        $hash = hash('SHA256', $data);
        return $hash;
    }

    /**
     * @param $data
     * @return bool
     */
    public function isValid($data)
    {
        if (empty($data)) {
            throw new \Exception('Data parameter is empty', Constants::EXCEPTION_INVALID_DATA);
        }
        $plain = base64_decode($data);
        list($salt, $numeric, $hash) = explode(':', $plain);

        $firstParts = "$salt:$numeric";

        if ((($numeric % $this->getPrime()) == 0)
            && ($this->getHash($firstParts) == $hash)) {
            return true;
        }

        return false;
    }

    /**
     * @param $key
     * @param $parms
     * @return string
     */
    protected function getDigest($key, $parms)
    {
        $data = join('', $parms);

        $digest = base64_encode(hash_hmac('sha256', $data, $key));
        return $digest;
    }

    /**
     * @param $digest
     * @param $key
     * @param mixed ...$parms
     * @return bool
     * @throws \Exception
     */
    public function validateDigest($digest, $key, ...$parms)
    {
        $newDigest = $this->getDigest($key, $parms);
        if ($digest === $newDigest) {
            return true;
        } else {
            throw new \Exception('Digest does not match', Constants::EXCEPTION_INVALID_DIGEST);
        }
    }
}
