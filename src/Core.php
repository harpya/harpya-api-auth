<?php

namespace harpya\api_auth;

class Core
{
    use MasterKey;
    use AuthToken;

    const PROP_OUTPUT_FILENAME = 'HARPYA_AUTH_OUTPUT_MASTER_FILE';

    protected $outputFilename;
    protected $prime;
    protected $salt;

    /**
     * @return mixed
     */
    public function getOutputFilename()
    {
        return $this->outputFilename;
    }

    /**
     * @param mixed $outputFilename
     * @return Core
     */
    public function setOutputFilename($outputFilename)
    {
        if (is_dir($outputFilename)) {
            $outputFilename .= '/master.key';
        }

        $this->outputFilename = $outputFilename;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPrime()
    {
        return  intval($this->prime->get());
    }

    /**
     * @param mixed $prime
     * @return Core
     */
    public function setPrime($prime)
    {
        $this->prime = $prime;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSalt()
    {
        return $this->salt->get();
    }

    /**
     * @param mixed $salt
     * @return Core
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;
        return $this;
    }

    /**
     * Core constructor.
     * @param array $props
     */
    public function __construct($props = [])
    {
        if (isset($props[self::PROP_OUTPUT_FILENAME])) {
            $this->setOutputFilename($props[self::PROP_OUTPUT_FILENAME]);
        }
    }
}
