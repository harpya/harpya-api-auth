<?php

namespace harpya\api_auth;

class HarpyaAuthClient
{
    protected $command;
    protected $params = [];

    /**
     * @throws \Exception
     */
    public function run()
    {
        $this->setup();

        switch ($this->command) {
            case 'init':
                $this->initializeMasterKey();
                break;

            case 'token:new':
                $this->createNewToken();
                break;
            default:
                $this->showHelp();
        }
    }

    /**
     * Helper function to get the parameters from CLI
     */
    protected function getArgs()
    {
        if (isset($argv)) {
            $this->params = $argv;
        } elseif (isset($_SERVER['argv'])) {
            $this->params = $_SERVER['argv'];
        } else {
            $this->out('Was not possible to load the CLI');
            exit(1);
        }
    }

    /**
     *
     */
    protected function setup()
    {
        $this->getArgs();

        $this->command = strtolower($this->params[1]);
    }

    /**
     * @param $value
     */
    protected function out($value)
    {
        echo "$value\n";
    }

    /**
     * @return array|false|string
     */
    protected function getOutputFilename()
    {
        $outputFilename = getenv(Core::PROP_OUTPUT_FILENAME);

        if (!$outputFilename) {
            $outputFilename = __DIR__ . '/../tmp/master.key';
        } elseif (is_dir($outputFilename)) {
            $outputFilename .= '/master.key';
        }

        return $outputFilename;
    }

    /**
     * @return Core
     */
    protected function getCore()
    {
        $outputFilename = $this->getOutputFilename();

        $core = new Core([Core::PROP_OUTPUT_FILENAME => $outputFilename
        ]);
        return $core;
    }

    /**
     * @throws \Exception
     */
    protected function initializeMasterKey()
    {
        $this->getCore()->createMasterKey();
        $this->out('Master key created on ' . $this->getOutputFilename());
    }

    /**
     * @throws \Exception
     */
    protected function createNewToken()
    {
        $core = $this->getCore()->loadMasterKey();

        $token = $core->createNewToken();
        $this->out("New token = $token");
    }

    /**
     * Display a help text
     */
    protected function showHelp()
    {
        $lines = [
            'Help',
            'Commands:',
            '   init : initialize a new masterkey file on ENV HARPYA_AUTH_OUTPUT_MASTER_FILE setting',
            '   token:new : generate a new token',
            '   help : show this page'
        ];

        foreach ($lines as $line) {
            $this->out($line);
        }
    }
}
