<?php

namespace  harpya\api_auth;

/**
 * Shields a property to avoid leak on output such as print_r or var_dump
 *
 * Class HiddenValue
 * @package harpya\api_auth
 */
class HiddenValue
{
    protected $value;

    /**
     * HiddenValue constructor.
     * @param bool $value
     */
    public function __construct($value = false)
    {
        $this->set($value);
    }

    /**
     * @param $value
     */
    public function set($value)
    {
        $this->value = $value;
    }

    /**
     * @return mixed
     */
    public function get()
    {
        return $this->value;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return '';
    }
}
