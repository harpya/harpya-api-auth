<?php

namespace harpya\api_auth;

use Primes\Primes;

/**
 * Trait MasterKey
 * @package harpya\api_auth
 */
trait MasterKey
{
    /**
     * @return string
     * @throws \Exception
     */
    public function createMasterKey()
    {
        if (!$this->getOutputFilename()) {
            throw  new \Exception('Output file was not specified. Aborting');
        }

        $masterKey = $this->generateMasterKey();
        $obj = base64_encode(serialize($masterKey));

        $this->persistsMasterKeyOnDisk($obj);

        return md5($obj);
    }

    /**
     * @return array
     * @throws \Exception
     */
    protected function generateMasterKey()
    {
        $prime = $this->createPrimeKey();
        $salt = new HiddenValue(bin2hex(random_bytes(32)));
        $masterKey = [
            'prime' => $prime,
            'salt' => $salt->get()
        ];
        return $masterKey;
    }

    /**
     * @param $obj
     * @throws \Exception
     */
    protected function persistsMasterKeyOnDisk($obj)
    {
        if (!file_exists(dirname($this->getOutputFilename()))) {
            throw new \Exception('Folder ' . dirname($this->getOutputFilename()) . ' does not exists', Constants::EXCEPTION_DIR_MASTER_KEY_DOES_NOT_EXISTS);
        }

        if (is_writable(dirname($this->getOutputFilename()))) {
            if (file_exists($this->getOutputFilename())) {
                if (is_writable($this->getOutputFilename())) {
                    file_put_contents($this->getOutputFilename(), $obj);
                } else {
                    throw new \Exception('File ' . $this->getOutputFilename() . ' is read-only', Constants::EXCEPTION_FILE_MASTER_KEY_READONLY);
                }
            } else {
                file_put_contents($this->getOutputFilename(), $obj);
            }
        } else {
            throw new \Exception('Folder ' . dirname($this->getOutputFilename()) . ' is read-only', Constants::EXCEPTION_DIR_MASTER_KEY_READONLY);
        }
    }

    /**
     * @return HiddenValue
     */
    public function createPrimeKey()
    {
        $primes = new Primes();
        $values = $primes->giveMePrimesBetween(1, 10000);
        $i = rand(0, count($values));
        $this->setPrime(new HiddenValue($values[$i]));
        return $this->getPrime();
    }

    /**
     * @return HiddenValue
     * @throws \Exception
     */
    public function createSaltKey()
    {
        $salt = random_bytes(12);
        return new HiddenValue(bin2hex($salt));
    }

    /**
     * @throws \Exception
     */
    public function loadMasterKey()
    {
        if (!$this->getOutputFilename()) {
            throw  new \Exception('Output file was not specified. Aborting');
        } elseif (!file_exists($this->getOutputFilename())) {
            throw  new \Exception('Master key file does not exists. Please, create if before proceed');
        }

        $contents = file_get_contents($this->getOutputFilename());

        $data = \unserialize(base64_decode($contents));

        $this->setPrime(new HiddenValue($data['prime']));
        $this->setSalt(new HiddenValue($data['salt']));

        return $this;
    }
}
