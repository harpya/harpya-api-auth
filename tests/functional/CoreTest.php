<?php

namespace tests\functional;

use harpya\api_auth\Constants;
use harpya\api_auth\Core;

class CoreTest extends \PHPUnit\Framework\TestCase
{
    public function getOutputFilename()
    {
        return __DIR__ . '/../../tmp/master.key';
    }

    public function testCreateMasterKey()
    {
        $core = new Core([Core::PROP_OUTPUT_FILENAME => $this->getOutputFilename()]);

        // Create a file with master key, a master secret string and used to encode

        $core->createMasterKey();

        $this->assertFileExists($this->getOutputFilename());
    }

    public function testCreateSaltKey()
    {
        $core = new Core();

        $salt = $core->createSaltKey();

        $this->assertTrue(is_string($salt->get()));
        $this->assertTrue(strlen($salt->get()) > 10);
    }

    public function testCreatePrimeKey()
    {
        $core = new Core();

        $prime = $core->createPrimeKey();

        $this->assertTrue(is_integer($prime));
    }

    /**
     * @depends testCreateMasterKey
     */
    public function testGenerateTokenWithMasterKey()
    {
        $core = new Core([Core::PROP_OUTPUT_FILENAME => $this->getOutputFilename()]);

        $core->loadMasterKey();

        $token = $core->createNewToken();

        $this->assertTrue(is_string($token));

        $decoded = base64_decode($token);
        $parts = explode(':', $decoded);

        $this->assertCount(3, $parts);
        $this->assertEquals(intval($parts[1]), $parts[1]);
    }

    /**
     * @depends testCreateMasterKey
     */
    public function testGenerateAndTestToken()
    {
        $core = new Core([Core::PROP_OUTPUT_FILENAME => $this->getOutputFilename()]);

        $core->loadMasterKey();

        $token = $core->createNewToken();

        $this->assertTrue(is_string($token));
        $this->assertTrue($core->isValid($token));
    }


    protected function getDigest($call,$timestamp, $key) {
        $digest = base64_encode(hash_hmac('sha256', $call.$timestamp, $key));
        //echo "\n -> ".$call.$timestamp;
        return $digest;
    }

    /**
     * @depends testCreateMasterKey
     */
    public function testValidHmacDigest() {

        $timestamp = date('d M Y H:i:s');
        $call = 'POST /api/v1/user';


        $core = new Core([Core::PROP_OUTPUT_FILENAME => $this->getOutputFilename()]);

        $core->loadMasterKey();

        $key = $core->createNewToken();

        $digest = $this->getDigest($call, $timestamp, $key);

//        echo "\n Digest = $digest ";

        $resp = $core->validateDigest($digest, $key, $call, $timestamp);

        $this->assertTrue($resp);

    }


    public function testInvalidHmacDigest() {
        $timestamp = date('d M Y H:i:s');
        $call = 'POST /api/v1/user';


        $core = new Core([Core::PROP_OUTPUT_FILENAME => $this->getOutputFilename()]);

        $core->loadMasterKey();

        $key = $core->createNewToken();

        $digest = $this->getDigest($call, $timestamp, $key);

        sleep(2);
        $newTimestamp = date('d M Y H:i:s');

        $this->expectExceptionCode(Constants::EXCEPTION_INVALID_DIGEST);
        $core->validateDigest($digest, $key, $call, $newTimestamp);
    }
}
